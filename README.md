# lsqfit-examples
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/millernb%2Flsqfit-examples/HEAD?filepath=examples/correlator_fit.ipynb)

This repo contains usage examples for `lsqfit`. 

1. The `examples/correlator_fit.ipynb` notebook uses the `Multifitter` class to fit a pair of baryon (or meson) correlators, each correlator corresponding to a different sink. This notebook also demonstrates how to efficiently bootstrap using `gv.switch_gvar() / gv.restore_gvar()`.

2. There is additionally an example of a chiral extrapolation of $`F_K/F_\pi`$ in the `examples/xpt_extrapolation.ipynb` notebook (see [arXiv:2005.04795](https://arxiv.org/abs/2005.04795) for more details). A Binder link is available [here](https://mybinder.org/v2/gl/millernb%2Flsqfit-examples/HEAD?filepath=examples/xpt_extrapolation.ipynb). 

## Requirements

Install requirements with 
```
pip --user -r requirements.txt
```
